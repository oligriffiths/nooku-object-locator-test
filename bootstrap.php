<?php

//Require composer autoloader
require_once __DIR__.'/vendor/autoload.php';

$config = [
    'debug'           =>  true,
    'cache'           =>  false,
    'cache_namespace' =>  'nooku',
    'base_path'       =>  __DIR__
];

//Setup class loader
$config['class_loader'] = Nooku\Library\ClassLoader::getInstance($config);

//Create the object manager
Nooku\Library\ObjectManager::getInstance($config);
